var express = require('express');
var expressLayouts = require('cloud/express-layouts');
var routes = require('cloud/routes');
var app = express();

app.set('views', 'cloud/views');
app.set('view engine', 'ejs');
app.use(express.bodyParser());
app.use(expressLayouts);
app.use(express.methodOverride());

app.locals.parseApplicationId = 'GtY8ZoigI48JFPLUOgXozQFVoD40wQGTXyrAI9L4';
app.locals.parseJavascriptKey = 'sSWRbT9KLDY4rOaKOL7ETghtU6Yt6NgnfDw2C1i6';
// TODO: change FB App Id
app.locals.facebookApplicationId = '230514897145786';

// Setup underscore to be available in all templates
app.locals._ = require('underscore');

app.get('/', routes.reader);

app.listen();
