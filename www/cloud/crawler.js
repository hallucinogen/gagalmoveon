var htmlparser = require('cloud/htmlparser.js');
var select = require('cloud/soupselect.js').select;
var _ = Parse._;

var Image = Parse.Object.extend('Image');

var crawl = function(url, selector, callback) {
  return Parse.Cloud.httpRequest({
    url: url
  }).then(function(response) {
    var result = [];
    var handler = new htmlparser.DefaultHandler(function(error, dom) {
      if (error) {
        return Parse.Promise.error();
      }
      result = select(dom, selector);
    });
    var parser = new htmlparser.Parser(handler);
    parser.parseComplete(response.text);

    return callback(result);
  });
}

var instagram = exports.instagram = {
  fetchImages: function(url) {
    var pageCount = 0;
    return crawl(url, 'script', function(elements) {
      var key = 'window._sharedData =';
      var promises = [];
      // fetch parallel
      for (var i = 0; i < elements.length; ++i) {
        var element = elements[i];
        var str = element.children && element.children[0] && element.children[0].data;
        if (!str || str.indexOf(key) == -1) continue;

        var jsonString = str.substr(str.indexOf(key) + key.length + 1);
        jsonString = jsonString.trim();
        jsonString = jsonString.slice(0, -1);

        var obj = JSON.parse(jsonString);
        var medias = obj.entry_data.UserProfile[0].userMedia;

        _.each(medias, function(media) {
          var imageUrl = media.images.standard_resolution.url;

          var image = new Image();
          image.set('url', imageUrl);
          promises.push(image.save());
        });

      }

      return Parse.Promise.when(promises);
    });
  }
}
