exports.reader = function(req, res) {
  var query = new Parse.Query('Image');
  return query.find().then(function(images) {
    console.log(images);
    res.render('reader', {
      title: 'Patah hati alangkah sakitnya...',
      images: images
    });
  });
}
